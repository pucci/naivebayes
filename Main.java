import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;

public class Main {
    private static String conjunto_de_dados;
    public static int number_of_entries = 1578627;
    private static SynchronizedCounter sync = null;
    private static HashSet<String> stop_Words = new HashSet<String>();
    
    public static void run_Crossvalidation(HashSet<String>stopwords){
	for(int i = 0; i<10;i++){
	    File file = new File("testing_"+i+".csv");
		if (file.exists()) {
		    file.delete();
		}    
	}
	
	// ********* Random line generator for method ***********
	int[] all_lines = new int[number_of_entries];
	HashSet<Integer>[] listas = new HashSet[(10)];
	
	// Array de threads Crossvalidation
	Crossvalidation[] cross = new Crossvalidation[10];
	Random random = new Random();
	
	// vetor com todos os numeros de registros
	for (int i = 0; i < number_of_entries; i++){
	    all_lines[i]=i+1;
	}
	
	//Escolhe 1 registro aleatoriamente
	int rand = random.nextInt(Main.number_of_entries);
	
	// para cada thread, alocar uma parti��o aleat�ria de registros para teste
	for (int count = 0; count < 10; count++) {
	    listas[count] = new HashSet<Integer>();
	    for (int num_lines = 0; num_lines < number_of_entries / 10; num_lines++) {
		while (all_lines[rand] == -1) {
		    rand = random.nextInt(Main.number_of_entries);
		}
		listas[count].add(all_lines[rand]);
		all_lines[rand] = -1;
	    }
	}
	
	// ************ Thread initializer *************
	for (int count = 0; count < 10; count++){
	    cross[count] = new Crossvalidation(count, listas[count],sync,stopwords);
	    cross[count].start();
	    // m�dia da acr�cia das threads
	}
	for (int count = 0; count < 10; count++) {
	    try {
		cross[count].join();
	    } catch (InterruptedException e) {
		e.printStackTrace();
	    }
	}
    }
    
    public static void run_Holdout(HashSet<String>stopwords){
	File file = new File("testing_Holdout.csv");
	if (file.exists()) {
	    file.delete();
	}
	Holdout ho = new Holdout();
	ho.run(conjunto_de_dados,stopwords);
    }
    
    public static void remove_Stop_words() {
	File stop = new File("stopwords_en.txt");
	Scanner scr;
	try {
	    scr = new Scanner(stop);
	    while (scr.hasNextLine()) {
		stop_Words.add(scr.nextLine());
	    }
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	}
    }

    /**
     * Main method to run and test
     */
    public static void main(String args[]) {
	long ini_time = System.currentTimeMillis();
	conjunto_de_dados=args[0];
	remove_Stop_words();
	
	
	System.out.println("Rodando Holdout ...");
	run_Holdout(null);
	System.out.println("Holdout time: " + (System.currentTimeMillis() - ini_time)/ 1000 + " seconds");                                 
	
	ini_time = System.currentTimeMillis();
	System.out.println("Rodando Holdout Sem stopwords ...");
	run_Holdout(stop_Words);
	System.out.println("Holdout time: " + (System.currentTimeMillis() - ini_time)/ 1000 + " seconds");
	
	ini_time = System.currentTimeMillis();
	System.out.println("Rodando Crossvalidation ...");
	sync = new SynchronizedCounter();
	run_Crossvalidation(null);
	System.out.println("Crossvalidation average accuracy is: "+sync.average_Accu());
	System.out.println("Time: " + (System.currentTimeMillis() - ini_time)
		/ 1000 + " seconds");
    }
}
