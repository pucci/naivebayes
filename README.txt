Aviso:
Todos os arquivos .java devem estar em um mesmo diret�rio.
Neste diret�rio tamb�m � necess�rio ter o arquivo "stopwords_en.txt" que cont�m as stopwords usadas pelo programa.
Neste diret�rio tamb�m deve estar contido a base de dados, arquivo "Sentiment_Analysis_Dataset.csv"(com o header) que deve ser passado como par�metro na hora da execu��o do problema.

Para compilar:
javac *.java

Para executar:
java Main <base_de_dados>
Exemplo:
java Main Sentiment_Analysis_Dataset.csv

Observa��es:
Na seguinte m�quina:
Windows 7 Pro 64bits
Intel core i5-3330 3.00Ghz
16 GB de RAM
HD 1TB de 7200 rpm
Java 1.6

O programa leva em m�dia 40 segundos para executar cada holdout e 140 segundos para executar crossvalidation.

