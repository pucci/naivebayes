import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

public class NaiveBayes {
    // HashMap that contains all positive sentiment tweets of the training set.
    private HashMap<String, Integer> twitter_Positive = new HashMap<String, Integer>();

    // HashMap that contains all negative sentiment tweets of the training set.
    private HashMap<String, Integer> twitter_Negative = new HashMap<String, Integer>();

    private int qtdP = 0;
    private int qtdN = 0;
    private String testing = null;
    
    private int[] matriz_confusao = new int[4]; //verdadeiro positivo, falso positivo, verdadeiro negativo/falso negativo;
    
    public NaiveBayes(HashMap<String, Integer> twitter_Positive,
	    HashMap<String, Integer> twitter_Negative, int qtdP, int qtdN,String testing) {
	super();
	this.twitter_Positive = twitter_Positive;
	this.twitter_Negative = twitter_Negative;
	this.qtdP = qtdP;
	this.qtdN = qtdN;
	this.testing = testing;
    }

	public double naive() throws FileNotFoundException{
		//hash que guarda posterioris
		HashMap<String, Double> posP = new HashMap<String,Double>();
		HashMap<String, Double> posN = new HashMap<String,Double>();

		//n�mero total de palavras de cada classe incluindo repeti��es
		//itera sobre array e vai somando em np/nn
		double nP = 0;
		double nN = 0;
		for(String chave: this.twitter_Positive.keySet()){
			int quantidade = this.twitter_Positive.get(chave);
			nP+= quantidade;
		}
		for(String chave: this.twitter_Negative.keySet()){
			int quantidade = this.twitter_Negative.get(chave);
			nN+= quantidade;
		}
		
		//a priori de cada classe
		//propor��o de cada classe do conjunto de treinamento
		double prioriP = Math.log10((double)this.qtdP/(double)(this.qtdP+this.qtdN)); //positivos/total
		double prioriN = Math.log10((double)this.qtdN/(double)(this.qtdP+this.qtdN)); //negativos/total
		
		
		//calculo das posterioris do conjunto de treinamento
		//salva como log(x) porque multiplica��o de numeros muito pequenos pode dar underflow no double
		//e como log(xy) = log(x) + log(y), no final as posterioris ser�o somadas ao inves de multiplicadas
		for(String chave: this.twitter_Positive.keySet()){
			double posteriori = Math.log10((double)this.twitter_Positive.get(chave)/(double)nP);
			posP.put(chave, posteriori);
		}
		for(String chave: this.twitter_Negative.keySet()){
			double posteriori = Math.log10((double)this.twitter_Negative.get(chave)/(double)nN);
			posN.put(chave, posteriori);
		}
		
		//a cada tweet que acertar a classifica��o adiciona um e depois usa pra calcular acur�cia
		int acertos = 0;
		int num_testes =0;
		File file = new File(this.testing);
		Scanner scr = new Scanner(new FileInputStream(file));
		
		while(scr.hasNextLine()){ //itera cada linha do conjunto de testes
			String linha = scr.nextLine();
			if(linha.isEmpty())break;
			num_testes++;
			String[] split = linha.split(",",4);
			double resultadoP = prioriP;
			double resultadoN = prioriN;
			int classeEsperada = Integer.parseInt(split[1]); // 1 se for positivo, 0 se for negativo, usado pra comparar e calcular acur�cia
			int classeObtida = -1; //resutlado da classifica�ao
			Scanner scr2 = new Scanner(split[3]);
			while(scr2.hasNext()){ //itera cada palavra do tweet
				String word = scr2.next();
				if(posP.get(word)!=null){
					resultadoP+=posP.get(word);
					resultadoN+=posN.get(word);
				}
			}
			scr2.close();
			if(resultadoP >= resultadoN)
				classeObtida = 1;
			else
				classeObtida = 0;
			
			if (classeObtida == classeEsperada) acertos++;
			
			if(classeObtida ==1 && classeEsperada == 1)
			    this.matriz_confusao[0]++;
			if(classeObtida ==1 && classeEsperada == 0)
			    this.matriz_confusao[1]++;
			if(classeObtida ==0 && classeEsperada == 0)
			    this.matriz_confusao[2]++;
			if(classeObtida ==0 && classeEsperada == 1)
			    this.matriz_confusao[3]++;
		}
		scr.close();
		double accuracy = (double)acertos/(double)(num_testes);
		
		System.out.println("Matriz confus�o");
		System.out.println(this.matriz_confusao[0]+"|"+ this.matriz_confusao[3] );
		System.out.println(this.matriz_confusao[1]+"|"+ this.matriz_confusao[2] );

		return accuracy;
	}
}
