import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;


public class Crossvalidation extends Thread{
    private final String database = "Sentiment_Analysis_Dataset.csv";
    SynchronizedCounter cross_Accu=null;
    
    private int thread_ID;
    // HashMap that contains all positive sentiment tweets of the training set.
    public HashMap<String, Integer> twitter_Positive = new HashMap<String, Integer>();

    // HashMap that contains all negative sentiment tweets of the training set.
    public HashMap<String, Integer> twitter_Negative = new HashMap<String, Integer>();
    
    // 	Array of random entry selections
    public HashSet<Integer> testing_Records = new HashSet<Integer>();
    
    public HashSet<String> linhas_de_treinamento = new HashSet<String>();
    
    public HashSet<String> stopWords = null; 
    public String testing = null;
    
    private double accu;
    private NaiveBayes nb;
    
    // Proportion of each class
    public int qtdP = 0;
    public int qtdN = 0;
    
    public Crossvalidation(int thread_ID,HashSet<Integer>training_list,SynchronizedCounter accu,HashSet<String>stop) {
	super();
	this.thread_ID = thread_ID;
	this.testing_Records = training_list;
	this.cross_Accu = accu;
	this.testing = "testing_"+thread_ID+".csv";
	this.stopWords = stop;
    }
   
   /*
    * This method receives a tweet and the associated sentiment of that tweet.
    * Each word of the input is then stored in a HashMap.
    * 
    * @param sentiment
    * @param tweet
    */
   public void add_words_to_hash(String sentiment, String tweet) {
	int value = 1;
	String word = null;
	Scanner scr = new Scanner(tweet);
	if (sentiment.equalsIgnoreCase("1")) {
	    qtdP++;
	    while (scr.hasNext()) {
		word = scr.next();
		if (twitter_Positive.containsKey(word)) {
		    value = twitter_Positive.get(word);
		    value++;
		}
		twitter_Positive.put(word, value);
		value=1;
		if (!twitter_Negative.containsKey(word)) {
		    twitter_Negative.put(word, 0);
		}
	    }

	} else {
	    qtdN++;
	    while (scr.hasNext()) {
		word = scr.next();
		if (twitter_Negative.containsKey(word)) {
		    value = twitter_Negative.get(word);
		    value++;
		}
		twitter_Negative.put(word, value);
		value=1;
		if (!twitter_Positive.containsKey(word)) {
		    twitter_Positive.put(word, 0);
		}
	    }
	}
	scr.close();
   }
    
    
    
   /**
    * This method receives a csv (comma separated value) file path and copies
    * each line to a specified output file.
    * 
    * @param input_CSV_File
    */
   public void read_CSV() {
	try {
	    Scanner scr = new Scanner(new FileInputStream(database),"UTF8");
	    // Header: ItemID, Sentiment, SentimentSource, SentimentText
	    String line = scr.nextLine();// Read and discard csv header
	    while (scr.hasNextLine()) {
		line = scr.nextLine().toLowerCase();
		String[] split = line.split(",", 4); // Splits string by comma
		split[3] = split[3].replaceAll("[,]", "");
		split[3] = split[3].replaceAll("\\s+", " ");
		if (!testing_Records.contains(new Integer(split[0]))) {
		    // adds the text from training file to the appropriate
		    // hashmap
		    
		    add_words_to_hash(split[1], split[3]);
		} else {
		    if (linhas_de_treinamento.size() < 1000) {
			linhas_de_treinamento.add(line);
		    } else {
			// entry was chosen for training
			StringBuilder s = new StringBuilder(split[0]+","+split[1]+","+split[2]+","+split[3]);
			write_to_File(testing,linhas_de_treinamento);
			linhas_de_treinamento.clear();
			linhas_de_treinamento.add(s.toString());
		    }
		}
	    }
	    if (this.stopWords != null && !this.stopWords.isEmpty()) {
		for (String chave : this.stopWords) {
		    twitter_Positive.remove(chave);
		    twitter_Negative.remove(chave);
		}
	    }
	    scr.close();
	    write_to_File(testing,linhas_de_treinamento);
	} catch (FileNotFoundException e2) {
	    System.err.print("File not found.");
	    e2.printStackTrace();
	} catch (Exception e1) {
	    System.err.print("Error reading csv file." + e1.getMessage());
	}
   }
    
    
   /**
    * This method generates a file for training of the algorithm and one for
    * testing.
    * 
    * @param word
    */
   private void write_to_File(String file,HashSet<String> text) {
	BufferedWriter writer = null;
	try {
	    writer = new BufferedWriter(new FileWriter(file, true));
	    for (Iterator<String> itr = text.iterator(); itr.hasNext();) {
		String line = itr.next();
//		String[] split =line.split(",", 4);
//		if (split[1].equalsIgnoreCase("1")) qtdP++;
//		else qtdN++;
		
		writer.write(line.toLowerCase().replaceAll("\\s+", " "));
		writer.newLine();
	    }
	    writer.close();
	} catch (Exception e) {
	    e.printStackTrace();
	}
   }
    
   /**
    * La place -
    */
   public void la_place() {
	// Set<String>temp_positive = twitter_Positive.keySet();
	for (Iterator<String> itr = twitter_Positive.keySet().iterator(); itr
		.hasNext();) {
	    String word = itr.next();
	    Integer value = twitter_Positive.get(word);
	    value++;
	    // twitter_Positive.remove(word);
	    twitter_Positive.put(word, value);
	}

	// Set<String>temp_negative = twitter_Negative.keySet();
	for (Iterator<String> itr = twitter_Negative.keySet().iterator(); itr
		.hasNext();) {
	    String word = itr.next();
	    Integer value = twitter_Negative.get(word);
	    value++;
	    // twitter_Negative.remove(word);
	    twitter_Negative.put(word, value);
	}
   }

    public void run() {
	long iniTime = System.currentTimeMillis();
	try {
	    read_CSV();
	    la_place();
	    nb = new NaiveBayes(twitter_Positive,twitter_Negative,qtdP,qtdN,testing);
	    try{
		accu = nb.naive();
	    }
	    catch (Exception e) {
		e.printStackTrace();
	    }
	} finally {
	    System.out.println("Finished thread: "+ this.thread_ID+" with accuracy of: "+ this.accu);
	    System.out.println("Elapsed Time for thread: "+ this.thread_ID+" is: "+(System.currentTimeMillis()-iniTime)/1000);
	    this.cross_Accu.add_thread_accu(this.accu);
	} // fim da thread
    }
    
    // =============== Getters & Setters ===============================
    public int getThread_ID() {
        return thread_ID;
    }

    public void setThread_ID(int thread_ID) {
        this.thread_ID = thread_ID;
    }

    public HashMap<String, Integer> getTwitter_Positive() {
        return twitter_Positive;
    }

    public HashMap<String, Integer> getTwitter_Negative() {
        return twitter_Negative;
    }

    public HashSet<Integer> getTraining_Records() {
        return testing_Records;
    }

    public void setTraining_Records(HashSet<Integer> training_Records) {
        this.testing_Records = training_Records;
    }

    public double getAccu() {
        return accu;
    }

    public void setAccu(double accu) {
        this.accu = accu;
    }
}
