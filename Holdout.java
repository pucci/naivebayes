import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;


public class Holdout {
    // HashMap that contains all positive sentiment tweets of the training set.
    public  HashMap<String, Integer> twitter_Positive = new HashMap<String, Integer>();

    // HashMap that contains all negative sentiment tweets of the training set.
    public  HashMap<String, Integer> twitter_Negative = new HashMap<String, Integer>();

    // Array of random entry selections
    public  HashSet<Integer> training_Records = new HashSet<Integer>();

    public  HashSet<String> linhas_de_treinamento = new HashSet<String>();

    // testing set file path
    public  String testing_Path = "testing_Holdout.csv";
    
    // Stopwords
    private HashSet<String> stopWords=null;

    // Proportion of each class
    public  int qtdP = 0;
    public  int qtdN = 0;

    // Hard Coded value of the number of entries in the database
    public int number_of_entries = 1578627;
    
    private double accu;
    
    /**
     * This method receives a csv (comma separated value) file path and copies
     * each line to a specified output file.
     * 
     * @param input_CSV_File
     */
    public void read_CSV(String input_CSV_File) {
	try {
	    Scanner scr = new Scanner(new FileInputStream(input_CSV_File),
		    "UTF8");
	  
	    // Header: ItemID, Sentiment, SentimentSource, SentimentText
	    String line = scr.nextLine();// Read and discard csv header
	    while (scr.hasNextLine()) {
		line = scr.nextLine().toLowerCase();
		String[] split = line.split(",", 4); // Splits string by comma
		split[3] = split[3].replaceAll("[,]", "");
		split[3] = split[3].replaceAll("\\s+", " ");
		if (training_Records.contains(new Integer(split[0]))) {
		    // adds the text from training file to the appropriate
		    // hashmap
		    
		    add_words_to_hash(split[1], split[3]);
		} else {
		    if (linhas_de_treinamento.size() < 1000) {
			linhas_de_treinamento.add(line);
		    } else {
			// entry was chosen for training
			StringBuilder s = new StringBuilder(split[0] + ","+ split[1] + "," + split[2] + "," + split[3]);
			write_to_File(testing_Path, linhas_de_treinamento);
			linhas_de_treinamento.clear();
			linhas_de_treinamento.add(s.toString());
		    }
		}
	    }
	    write_to_File(testing_Path, linhas_de_treinamento);
	    
	    if (stopWords != null && !stopWords.isEmpty()) {
		for (String chave : stopWords) {
		    twitter_Positive.remove(chave);
		    twitter_Negative.remove(chave);
		}
	    }
	    scr.close();
	} catch (FileNotFoundException e2) {
	    System.err.print("File not found.");
	    e2.printStackTrace();
	} catch (Exception e1) {
	    System.err.print("Error reading csv file." + e1.getMessage());
	}
    }
    
    /**
     * This method receives a tweet and the associated sentiment of that tweet.
     * Each word of the input is then stored in a HashMap.
     * 
     * @param sentiment
     * @param tweet
     */
    public  void add_words_to_hash(String sentiment, String tweet) {
	int value = 1;
	String word = null;
	Scanner scr = new Scanner(tweet);
	if (sentiment.equalsIgnoreCase("1")) {
	    qtdP++;
	    while (scr.hasNext()) {
		word = scr.next();
		if (twitter_Positive.containsKey(word)) {
		    value = twitter_Positive.get(word);
		    value++;
		}
		twitter_Positive.put(word, value);
		value=1;
		if (!twitter_Negative.containsKey(word)) {
		    twitter_Negative.put(word, 0);
		}
	    }

	} else {
	    qtdN++;
	    while (scr.hasNext()) {
		word = scr.next();
		if (twitter_Negative.containsKey(word)) {
		    value = twitter_Negative.get(word);
		    value++;
		}
		twitter_Negative.put(word, value);
		value=1;
		if (!twitter_Positive.containsKey(word)) {
		    twitter_Positive.put(word, 0);
		}
	    }
	}
	scr.close();
    }
    
    /**
     * This method generates a file for training of the algorithm and one for
     * testing.
     * 
     * @param word
     */
    private  void write_to_File(String file, HashSet<String> text) {
	BufferedWriter writer = null;
	
	try {
	    FileWriter fw = new FileWriter(file, true);
	    writer = new BufferedWriter(fw);
	    for (Iterator<String> itr = text.iterator(); itr.hasNext();) {
		String line = itr.next();
		writer.write(line.toLowerCase().replaceAll("\\s+", " "));
		writer.newLine();
	    }
	    
	    writer.flush();
	    writer.close();
	    fw.close();
	} catch (Exception e) {
	    e.printStackTrace();
	}
	finally{
	    try {
		writer.close();
	    } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}
    }
    
    /**
     * La place -
     */
    public void la_place() {
	// Set<String>temp_positive = twitter_Positive.keySet();
	for (Iterator<String> itr = twitter_Positive.keySet().iterator(); itr
		.hasNext();) {
	    String word = itr.next();
	    Integer value = twitter_Positive.get(word);
	    value++;
	    // twitter_Positive.remove(word);
	    twitter_Positive.put(word, value);
	}

	// Set<String>temp_negative = twitter_Negative.keySet();
	for (Iterator<String> itr = twitter_Negative.keySet().iterator(); itr
		.hasNext();) {
	    String word = itr.next();
	    Integer value = twitter_Negative.get(word);
	    value++;
	    // twitter_Negative.remove(word);
	    twitter_Negative.put(word, value);
	}
    }
    
    /**
     * This method initiates an array that will contain random numbers which
     * represent random lines to be read from the original input csv to generate
     * the training database for the Naive Bayes algorithm.
     * 
     * Item 3.b of the specification
     */
    public void random_Lines() {
	// from specifications, the trainning DB will have 2/3 of all records
	int training = (this.number_of_entries*2) / 3;
	Random random = new Random();
	for (int i = 0; i < training; i++) {
	 // Random is [0,n[ thus 1 is added to final entry and eliminates entry 0
	 // that does not exist
	    int rand = random.nextInt(this.number_of_entries)+1; 
	 // If a record is already chosen 
	    while (this.training_Records.contains(rand)) { 
		rand = random.nextInt(this.number_of_entries)+1;
	    }
	    this.training_Records.add(rand);
	}
    }
    
    public void run(String file,HashSet<String>stop){
	try {
	    stopWords = stop;
	    random_Lines();
	    read_CSV(file);
	    la_place();
	    NaiveBayes nb = new NaiveBayes(twitter_Positive,twitter_Negative,qtdP,qtdN,testing_Path);
	    try{
		accu = nb.naive();
	    }
	    catch (Exception e) {
		e.printStackTrace();
	    }
	} finally {
	    if(stopWords != null && !stopWords.isEmpty())
	    {
		System.out.println("Finished holdout without stopwors with accuracy of: "+ this.accu);
	    }
	    else System.out.println("Finished holdout with accuracy of: "+ this.accu);
	} // fim do holdout
    }
}
