class SynchronizedCounter {
	private double cross_Accu = 0.0;

	synchronized void add_thread_accu(double accu) {
	    cross_Accu += accu;
	}

	synchronized double average_Accu() {
	    return cross_Accu / 10.0;
	}
    }
